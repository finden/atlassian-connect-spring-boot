package com.atlassian.connect.spring.internal.jpa;

import com.atlassian.connect.spring.AtlassianHostRepository;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.util.StringUtils;

/**
 * {@link EnableAutoConfiguration Auto-configuration} for Atlassian Connect add-ons using Spring Data JPA and Liquibase.
 */
@Configuration
@AutoConfigureAfter({ DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@AutoConfigureBefore(LiquibaseAutoConfiguration.class)
@EnableJpaRepositories(basePackageClasses = {AtlassianHostRepository.class})
@EnableJpaAuditing
@PropertySource("classpath:config/atlassian-connect-spring-boot-jpa-starter.properties")
public class AtlassianJpaAutoConfiguration {

    /**
     * Everything below is a bit of a hack. Basically we are trying to create a custom Liquibase migration task which has
     * access to the Spring application context. Given that Liquibase itself is not a Spring managed bean the only
     * way to do this is through a static accessor on a Spring managed class. This hack is making sure that this class
     * is initalized before Liquibase by manually making it a dependency of the 'liquibase' bean.
     */
    @Configuration
    @ConditionalOnClass(SpringLiquibase.class)
    @Import(LiquibaseApplicationContextDependencyConfiguration.class)
    public static class JpaApplicationContextConfiguration {
        @Bean
        public ApplicationContextProvider jpaApplicationContextProvider(ApplicationContext applicationContext) {
            return new ApplicationContextProvider(applicationContext);
        }
    }

    @Configuration
    protected static class LiquibaseApplicationContextDependencyConfiguration implements BeanFactoryPostProcessor {

        @Override
        public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
            try {
                BeanDefinition beanDefinition = beanFactory.getBeanDefinition("liquibase");
                String[] dependencies = beanDefinition.getDependsOn();
                beanDefinition.setDependsOn(StringUtils.addStringToArray(dependencies, "jpaApplicationContextProvider"));
            } catch (NoSuchBeanDefinitionException e) {
                // ignore
            }
        }
    }
}
