package com.atlassian.connect.spring.internal.jpa;

import org.springframework.context.ApplicationContext;

public class ApplicationContextProvider {

    private static ApplicationContext context;

    public ApplicationContextProvider(ApplicationContext applicationContext) {
        ApplicationContextProvider.context = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return context;
    }
}