package com.atlassian.connect.spring.internal.jpa.migration;

import liquibase.change.custom.CustomSqlChange;
import liquibase.database.Database;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.RawSqlStatement;
import liquibase.structure.core.Column;

import static com.atlassian.connect.spring.internal.data.AtlassianHostCompositeKeyAdvice.COMPOSITE_KEY_SEPARATOR;

public class MergeClientKeyAndAddonKeyToIdChange implements CustomSqlChange {

    private static final String ATLASSIAN_HOST_TABLE = "atlassian_host";
    private static final String ADDON_KEY_COLUMN = "addon_key";
    private static final String CLIENT_KEY_COLUMN = "client_key";

    @Override
    public SqlStatement[] generateStatements(Database database) throws CustomChangeException {
        String updateStatement =
                "UPDATE " + database.escapeTableName(database.getDefaultCatalogName(), database.getDefaultSchemaName(), ATLASSIAN_HOST_TABLE) +
                " SET " + database.escapeObjectName("id", Column.class)
                + " = " + database.getConcatSql(database.escapeObjectName(CLIENT_KEY_COLUMN, Column.class)
                , "'" + COMPOSITE_KEY_SEPARATOR + "'", database.escapeObjectName(ADDON_KEY_COLUMN, Column.class));

        return new SqlStatement[] {new RawSqlStatement(updateStatement)};
    }

    @Override
    public String getConfirmationMessage() {
        return String.format("Creating new composite key by combining '%s' and '%s'", CLIENT_KEY_COLUMN, ADDON_KEY_COLUMN);
    }

    @Override
    public void setUp() throws SetupException {
        // noop
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        // noop
    }

    @Override
    public ValidationErrors validate(Database database) {
        return new ValidationErrors();
    }
}
