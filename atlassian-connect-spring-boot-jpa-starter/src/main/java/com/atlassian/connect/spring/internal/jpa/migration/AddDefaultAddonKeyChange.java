package com.atlassian.connect.spring.internal.jpa.migration;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoaderRegistry;
import com.atlassian.connect.spring.internal.jpa.ApplicationContextProvider;
import liquibase.change.custom.CustomSqlChange;
import liquibase.database.Database;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.UpdateStatement;

import java.util.Optional;

public class AddDefaultAddonKeyChange implements CustomSqlChange {

    private static final String ATLASSIAN_HOST_TABLE = "atlassian_host";
    private static final String ADDON_KEY_COLUMN = "addon_key";

    private String defaultAddonKey;

    @Override
    public SqlStatement[] generateStatements(Database database) throws CustomChangeException {
        if (defaultAddonKey == null) {
            return new SqlStatement[0];
        }

        UpdateStatement updateStatement = new UpdateStatement(database.getDefaultCatalogName(), database.getDefaultSchemaName(), ATLASSIAN_HOST_TABLE);
        updateStatement.addNewColumnValue(ADDON_KEY_COLUMN, defaultAddonKey);
        return new SqlStatement[] {updateStatement};
    }

    @Override
    public String getConfirmationMessage() {
        if (defaultAddonKey == null) {
            return "No migration necessary as no default addon found";
        }
        return String.format("Updated '%s' for all rows in '%s' to '%s'", ADDON_KEY_COLUMN, ATLASSIAN_HOST_TABLE, defaultAddonKey);
    }

    @Override
    public void setUp() throws SetupException {
        // If there is an addon descriptor for the root path we use its addon key as the default for the migration
        AddonDescriptorLoaderRegistry addonDescriptorLoaderRegistry = ApplicationContextProvider.getApplicationContext().getBean(AddonDescriptorLoaderRegistry.class);
        Optional<AddonDescriptorLoader> addonDescriptorLoader = addonDescriptorLoaderRegistry.getByPath("");
        if (addonDescriptorLoader.isPresent()) {
            this.defaultAddonKey = addonDescriptorLoader.get().getDescriptor().getKey();
        }
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        // noop
    }

    @Override
    public ValidationErrors validate(Database database) {
        return new ValidationErrors();
    }
}
