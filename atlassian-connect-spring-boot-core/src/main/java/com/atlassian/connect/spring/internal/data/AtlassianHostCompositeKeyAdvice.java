package com.atlassian.connect.spring.internal.data;

import com.atlassian.connect.spring.AtlassianHost;
import org.apache.commons.lang3.Validate;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * Makes sure that anything saved using the {@link com.atlassian.connect.spring.AtlassianHostRepository} gets a
 * composite key base on clientKey and addonKey assigned before persisting
 */
@Aspect
public class AtlassianHostCompositeKeyAdvice {

    public static final String COMPOSITE_KEY_SEPARATOR = "::";

    @Before("execution(* com.atlassian.connect.spring.AtlassianHostRepository.save(*)) && args(host)")
    public void generateCompositeKey(AtlassianHost host) {
        host.setId(createCompositeKey(host));
    }

    @Before("execution(* com.atlassian.connect.spring.AtlassianHostRepository.save(*)) && args(hosts)")
    public void generateCompositeKeyForIterable(Iterable<?> hosts) {
        hosts.forEach((object) -> {
            if (object instanceof AtlassianHost) {
                AtlassianHost host = (AtlassianHost) object;
                host.setId(createCompositeKey(host));
            }
        });
    }

    public static String createCompositeKey(AtlassianHost host) {
        Validate.notNull(host.getClientKey(), "clientKey can't be null");
        Validate.notNull(host.getAddonKey(), "addonKey can't be null");
        return host.getClientKey() + COMPOSITE_KEY_SEPARATOR + host.getAddonKey();
    }
}
