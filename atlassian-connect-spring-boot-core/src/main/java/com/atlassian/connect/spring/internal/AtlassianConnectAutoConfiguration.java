package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.internal.auth.jwt.JwtAuthenticationFilter;
import com.atlassian.connect.spring.internal.data.AtlassianHostCompositeKeyAdvice;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoaderRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.authentication.AuthenticationManager;

import javax.servlet.Filter;
import java.io.FileNotFoundException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@link EnableAutoConfiguration Auto-configuration} for Atlassian Connect add-ons.
 */
@Configuration
@ComponentScan(basePackageClasses = {AtlassianConnectAutoConfiguration.class})
@EnableConfigurationProperties(AtlassianConnectProperties.class)
@EnableAsync
@EnableAspectJAutoProxy
public class AtlassianConnectAutoConfiguration {

    private static final String DEFAULT_DESCRIPTOR_FILENAME = "atlassian-connect.json";

    @Autowired
    private ConfigurableEnvironment configurableEnvironment;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private AtlassianConnectProperties atlassianConnectProperties;

    @Configuration
    @PropertySource("classpath:config/default.properties")
    static class Defaults {}

    @Configuration
    @Profile("production")
    @PropertySource({"classpath:config/default.properties", "classpath:config/production.properties"})
    static class Production {}

    @Bean
    public Filter jwtAuthenticationFilter(AuthenticationManager authenticationManager, ServerProperties serverProperties) {
        return new JwtAuthenticationFilter(authenticationManager, serverProperties);
    }

    @Bean
    public AtlassianHostCompositeKeyAdvice compositeKeyAdvice() {
        return new AtlassianHostCompositeKeyAdvice();
    }

    @Bean
    public AddonDescriptorLoaderRegistry configuredAddonDescriptorLoaders() throws FileNotFoundException {
        // Create loaders for the configured addons first
        Set<AddonDescriptorLoader> addonDescriptorLoaders = atlassianConnectProperties.getAddons().values().stream()
                .map(addon -> new AddonDescriptorLoader(addon.getContextPath(), addon.getDescriptor(), resourceLoader, configurableEnvironment))
                .collect(Collectors.toSet());

        // If there are none configured try to load the default descriptor
        if (addonDescriptorLoaders.isEmpty()) {
            if (new ClassPathResource(DEFAULT_DESCRIPTOR_FILENAME).exists()) {
                addonDescriptorLoaders.add(new AddonDescriptorLoader("", DEFAULT_DESCRIPTOR_FILENAME, resourceLoader, configurableEnvironment));
            }
        }

        return new AddonDescriptorLoaderRegistry(addonDescriptorLoaders);
    }

}
