package com.atlassian.connect.spring.internal;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Properties for Atlassian Connect add-ons.
 */
@ConfigurationProperties(prefix = "atlassian.connect")
public class AtlassianConnectProperties {

    /**
     * Accept installations signed by an unknown host. (Useful in development mode using an in-memory database.)
     */
    private boolean allowReinstallMissingHost = false;

    /**
     * Enable debug mode for the JavaScript API, loading all-debug.js instead of all.js.
     */
    private boolean debugAllJs = false;

    /**
     * Allow for configuration of other addons with non-default context-path and/or descriptor filename
     */
    private Map<String, AddonConfiguration> addons = new HashMap<>();

    public boolean isAllowReinstallMissingHost() {
        return allowReinstallMissingHost;
    }

    public void setAllowReinstallMissingHost(boolean allowReinstallMissingHost) {
        this.allowReinstallMissingHost = allowReinstallMissingHost;
    }

    public boolean isDebugAllJs() {
        return debugAllJs;
    }

    public void setDebugAllJs(boolean debugAllJs) {
        this.debugAllJs = debugAllJs;
    }

    public Map<String, AddonConfiguration> getAddons() {
        return addons;
    }

    public void setAddons(Map<String, AddonConfiguration> addons) {
        this.addons = addons;
    }

    public static class AddonConfiguration {
        private String contextPath;
        private String descriptor;

        public String getContextPath() {
            return contextPath;
        }

        public void setContextPath(String contextPath) {
            this.contextPath = contextPath;
        }

        public String getDescriptor() {
            return descriptor;
        }

        public void setDescriptor(String descriptor) {
            this.descriptor = descriptor;
        }
    }
}
