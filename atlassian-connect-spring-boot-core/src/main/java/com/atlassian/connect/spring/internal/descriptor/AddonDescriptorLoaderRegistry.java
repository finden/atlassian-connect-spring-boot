package com.atlassian.connect.spring.internal.descriptor;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;


public class AddonDescriptorLoaderRegistry {
    private final Set<AddonDescriptorLoader> addonDescriptorLoaders;

    public AddonDescriptorLoaderRegistry(Set<AddonDescriptorLoader> addonDescriptorLoaders) {
        this.addonDescriptorLoaders = addonDescriptorLoaders;
    }

    public Optional<AddonDescriptorLoader> getByPath(String path) {
        Stream<AddonDescriptorLoader> sorted = addonDescriptorLoaders.stream()
                .filter(addonDescriptorLoader -> path.startsWith(addonDescriptorLoader.getPath()))
                // Reverse sort to get more specific matches first
                .sorted((o1, o2) -> o2.getPath().compareToIgnoreCase(o1.getPath()));
        Optional<AddonDescriptorLoader> matchingAddonDescriptorLoader = sorted
                .findFirst();

        if (!matchingAddonDescriptorLoader.isPresent()) {
            return Optional.empty();
        }

        return Optional.of(matchingAddonDescriptorLoader.get());
    }

    public Optional<AddonDescriptorLoader> getByRequest(HttpServletRequest request) {
        return getByPath(request.getServletPath());
    }

    public AddonDescriptorLoader getByRequestQuietly(HttpServletRequest request) {
        Optional<AddonDescriptorLoader> addonDescriptorLoader = getByRequest(request);
        if (!addonDescriptorLoader.isPresent()) {
            throw new RuntimeException(String.format("No matching addon descriptor found for path '%s'", request.getServletPath()));
        }
        return addonDescriptorLoader.get();
    }

    public AddonDescriptorLoader getByRequestContextHolderQuietly() {
        Optional<AddonDescriptorLoader> optionalLoader = getByRequestContextHolder();
        if (!optionalLoader.isPresent()) {
            throw new RuntimeException("Unable to find addon. Either this is called from a non request thread or no addon exists for the current servlet path");
        }
        return optionalLoader.get();
    }

    public Optional<AddonDescriptorLoader> getByRequestContextHolder() {
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (sra == null) {
            return Optional.empty();
        }
        HttpServletRequest request = sra.getRequest();
        return getByRequest(request);
    }

    public Set<AddonDescriptorLoader> getAddonDescriptorLoaders() {
        return Collections.unmodifiableSet(addonDescriptorLoaders);
    }

}
