package com.atlassian.connect.spring.internal.descriptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.Arrays;

@Component
public class AddonDescriptorControllerHandlerMapping extends RequestMappingHandlerMapping {

    /**
     * An ordinal before that used by {@link RequestMappingHandlerMapping} (0).
     *
     * @see WebMvcConfigurationSupport#requestMappingHandlerMapping()
     */
    private static final int ORDER = -101;

    @Autowired
    private AddonDescriptorLoaderRegistry addonDescriptorLoaderRegistry;

    public AddonDescriptorControllerHandlerMapping() {
        setOrder(ORDER);
    }

    @Override
    protected boolean isHandler(Class<?> beanType) {
        return super.isHandler(beanType) && AddonDescriptorController.class.equals(beanType);
    }

    @Override
    protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
        String[] addonContextPaths = addonDescriptorLoaderRegistry.getAddonDescriptorLoaders().stream().map(addonDescriptorLoader -> addonDescriptorLoader.getPath()).toArray(size -> new String[size]);
        if (method.equals(AddonDescriptorController.getRootMethod())) {
            return RequestMappingInfo.paths(addonContextPaths).methods(RequestMethod.GET).build();
        }
        if (method.equals(AddonDescriptorController.getDescriptorMethod())) {
            String[] descriptorUrls = Arrays.asList(addonContextPaths).stream().map(path -> path + "/atlassian-connect.json").toArray(size -> new String[size]);
            return RequestMappingInfo.paths(descriptorUrls).methods(RequestMethod.GET).produces(MediaType.APPLICATION_JSON_VALUE).build();
        }
        return null;
    }
}
