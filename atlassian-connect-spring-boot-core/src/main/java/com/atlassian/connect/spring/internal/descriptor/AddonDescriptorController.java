package com.atlassian.connect.spring.internal.descriptor;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Optional;

/**
 * A controller that serves the Atlassian Connect add-on descriptor (<code>atlassian-connect.json</code>).
 */
@IgnoreJwt
@Controller
public class AddonDescriptorController {

    @Autowired
    private AddonDescriptorLoaderRegistry addonDescriptorLoaderRegistry;

    public static Method getRootMethod() {
        return ReflectionUtils.findMethod(AddonDescriptorController.class, "getRoot", HttpServletRequest.class);
    }

    public static Method getDescriptorMethod() {
        return ReflectionUtils.findMethod(AddonDescriptorController.class, "getDescriptor", HttpServletRequest.class);
    }

    public RedirectView getRoot(HttpServletRequest request) {
        Optional<AddonDescriptorLoader> addonDescriptorLoader = addonDescriptorLoaderRegistry.getByRequest(request);
        if (addonDescriptorLoader.isPresent()) {
            RedirectView redirectView = new RedirectView(addonDescriptorLoader.get().getPath() + "/atlassian-connect.json");
            redirectView.setHttp10Compatible(false);
            redirectView.setStatusCode(HttpStatus.FOUND);
            return redirectView;
        } else {
            throw new NotFoundException();
        }
    }

    public ResponseEntity getDescriptor(HttpServletRequest request) throws IOException {
        Optional<AddonDescriptorLoader> addonDescriptorLoader = addonDescriptorLoaderRegistry.getByRequest(request);
        if (addonDescriptorLoader.isPresent()) {
            return ResponseEntity.ok(addonDescriptorLoader.get().getRawDescriptor());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    private static class NotFoundException extends RuntimeException {}

}
