package com.atlassian.connect.spring.internal.descriptor;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * A loader of Atlassian Connect add-on descriptors, providing replacement of configuration placeholders.
 */
public class AddonDescriptorLoader {

    private final String path;

    private final String descriptorFilename;

    private final ConfigurableEnvironment configurableEnvironment;

    private final ObjectMapper mapper;

    private ResourceLoader resourceLoader;

    public AddonDescriptorLoader(String path, String descriptorFilename, ResourceLoader resourceLoader, ConfigurableEnvironment configurableEnvironment) {
        this.path = path;
        this.descriptorFilename = descriptorFilename;
        this.resourceLoader = resourceLoader;
        this.configurableEnvironment = configurableEnvironment;
        mapper = createObjectMapper();
    }

    public String getPath() {
        return path;
    }

    public AddonDescriptor getDescriptor() {
        String descriptor = getRawDescriptor();
        try {
            return mapper.readValue(descriptor, AddonDescriptor.class);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public String getRawDescriptor() {
        String rawDescriptor = getRawDescriptorQuietly();
        return configurableEnvironment.resolvePlaceholders(rawDescriptor);
    }

    private String getRawDescriptorQuietly() {
        try {
            return getDescriptorResourceContents();
        } catch (IOException e) {
            throw new AssertionError("Could not load add-on descriptor", e);
        }
    }

    private String getDescriptorResourceContents() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:" + descriptorFilename);
        if (!resource.exists()) {
            throw new IOException(String.format("No add-on descriptor found (%s)", descriptorFilename));
        }
        return StreamUtils.copyToString(resource.getInputStream(), Charset.defaultCharset());
    }

    private ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setVisibility(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        return mapper;
    }
}
