package com.atlassian.connect.spring.internal.descriptor;

import org.junit.Test;
import org.mockito.internal.util.collections.Sets;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddonDescriptorLoaderRegistryTest {

    @Test
    public void addonContextPathsWithSamePrefixGetMatchedInRightOrder() {
        AddonDescriptorLoader testLoader = new AddonDescriptorLoader("/test", null, null, null);
        AddonDescriptorLoader test2Loader = new AddonDescriptorLoader("/test2", null, null, null);
        AddonDescriptorLoaderRegistry addonDescriptorLoaderRegistry = new AddonDescriptorLoaderRegistry(Sets.newSet(testLoader, test2Loader));

        assertThat(addonDescriptorLoaderRegistry.getByPath("/test2/path").get(), is(test2Loader));
    }
}
