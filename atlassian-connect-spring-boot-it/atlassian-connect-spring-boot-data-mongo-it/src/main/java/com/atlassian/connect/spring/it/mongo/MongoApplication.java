package com.atlassian.connect.spring.it.mongo;

import com.atlassian.connect.spring.AtlassianHostRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = {AtlassianHostRepository.class})
public class MongoApplication {

    public static void main(String[] args) throws Exception {
        new SpringApplication(MongoApplication.class).run(args);
    }
}
