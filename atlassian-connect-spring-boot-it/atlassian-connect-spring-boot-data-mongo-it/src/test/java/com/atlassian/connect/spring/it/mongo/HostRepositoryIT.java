package com.atlassian.connect.spring.it.mongo;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(MongoApplication.class)
@WebAppConfiguration
public class HostRepositoryIT {

    public static final String CLIENT_KEY = "clientkey";
    public static final String ADDON_KEY = "addonkey";

    @Autowired
    private AtlassianHostRepository hostRepository;

    @After
    public void deleteAtlassianHosts() {
        hostRepository.deleteAll();
    }

    @Test
    public void shouldHostRepositoryBeEmpty() throws Exception {
        assertThat(hostRepository.count(), is(0L));
    }

    @Test
    public void shouldGenerateIdOnSave() throws Exception {
        AtlassianHost host = new AtlassianHost();
        host.setClientKey(CLIENT_KEY);
        host.setAddonKey(ADDON_KEY);
        hostRepository.save(host);

        assertThat(host.getId(), is(CLIENT_KEY + "::" + ADDON_KEY));
    }
}
