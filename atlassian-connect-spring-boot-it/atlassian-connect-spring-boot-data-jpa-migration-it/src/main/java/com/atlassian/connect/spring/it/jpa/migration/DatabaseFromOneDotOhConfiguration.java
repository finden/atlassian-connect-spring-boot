package com.atlassian.connect.spring.it.jpa.migration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * Use datasource initialized with 1.0 data instead of the default 'testdb'
 */
@Configuration
public class DatabaseFromOneDotOhConfiguration {
    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.HSQL)
            // Change the name to not interfere with the 'testdb' default database. Even when using different spring
            // context 'testdb' will still be in memory as the contexts are cached
            .setName("one-dot-oh")
            .addScript("classpath:data-1.0.sql")
            .build();
    }
}