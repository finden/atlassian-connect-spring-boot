package com.atlassian.connect.spring.it.jpa.migration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaMigrationApplication {

    public static void main(String[] args) throws Exception {
        new SpringApplication(JpaMigrationApplication.class).run(args);
    }
}
