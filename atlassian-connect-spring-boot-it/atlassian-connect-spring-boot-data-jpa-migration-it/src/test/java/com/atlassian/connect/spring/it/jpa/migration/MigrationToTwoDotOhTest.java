package com.atlassian.connect.spring.it.jpa.migration;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.internal.data.AtlassianHostCompositeKeyAdvice;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(JpaMigrationApplication.class)
@WebAppConfiguration
public class MigrationToTwoDotOhTest {

    // This is the key from the data-1.0.sql dump
    public static final String CLIENT_KEY = "some-host";

    @Value("${add-ons.root.key}")
    private String addonKey;

    @Autowired
    private AtlassianHostRepository hostRepository;

    @Test
    public void hostsHaveAddonKeyAfterMigration() {
        assertThat(hostRepository.count(), is(1L));
        AtlassianHost migratedHost = hostRepository.findAll().iterator().next();

        // Make sure that the addon key got added during migration
        assertThat(migratedHost.getAddonKey(), CoreMatchers.is(addonKey));

        // Make sure that the id is correct
        AtlassianHost dummyHost = new AtlassianHost();
        dummyHost.setClientKey(CLIENT_KEY);
        dummyHost.setAddonKey(addonKey);
        assertThat(migratedHost.getId(), is(AtlassianHostCompositeKeyAdvice.createCompositeKey(dummyHost)));
    }

}
