package com.atlassian.connect.spring.it;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AddonsConfiguration {

    @Bean(name = "testAddonConfiguration")
    @ConfigurationProperties(prefix = "add-ons.test")
    public AddonConfiguration testAddonConfiguration() {
        return new AddonConfiguration();
    }

    @Bean(name = "rootAddonConfiguration")
    @ConfigurationProperties(prefix = "add-ons.root")
    public AddonConfiguration rootAddonConfiguration() {
        return new AddonConfiguration();
    }

    public static class AddonConfiguration {
        private String key;
        private String name;
        private String contextPath;
        private String installedUrl;
        private String uninstalledUrl;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContextPath() {
            return contextPath;
        }

        public void setContextPath(String contextPath) {
            this.contextPath = contextPath;
        }

        public String getInstalledUrl() {
            return installedUrl;
        }

        public void setInstalledUrl(String installedUrl) {
            this.installedUrl = installedUrl;
        }

        public String getUninstalledUrl() {
            return uninstalledUrl;
        }

        public void setUninstalledUrl(String uninstalledUrl) {
            this.uninstalledUrl = uninstalledUrl;
        }
    }
}
