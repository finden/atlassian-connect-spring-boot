package com.atlassian.connect.spring.test.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.test.util.BaseApplicationTest;
import com.atlassian.connect.spring.test.util.SimpleJwtSigningRestTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.connect.spring.test.lifecycle.LifecycleBodyHelper.createLifecycleEventMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest
public class DevelopmentLifecycleControllerTest extends BaseApplicationTest {

    @Value("${add-ons.test.installed-url}")
    private String installedUrl;

    @Test
    public void shouldAcceptInstallFromUnknownHostInDevMode() throws Exception {
        AtlassianHost host = getDefaultHost();
        RestTemplate restTemplate = new SimpleJwtSigningRestTemplate(host, getAddonBaseUrl(), Optional.empty());
        ResponseEntity<String> response = restTemplate.postForEntity(URI.create(getAddonBaseUrl() + installedUrl),
                createLifecycleEventMap("installed", host), String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }
}
