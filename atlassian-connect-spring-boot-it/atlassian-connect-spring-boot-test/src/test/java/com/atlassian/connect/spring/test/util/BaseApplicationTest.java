package com.atlassian.connect.spring.test.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.jwt.JwtAuthentication;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoaderRegistry;
import com.atlassian.connect.spring.it.AddonsConfiguration;
import com.atlassian.connect.spring.it.AtlassianConnectTestApplication;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;
import java.util.Optional;

@SpringApplicationConfiguration(AtlassianConnectTestApplication.class)
@ActiveProfiles("addon-config-override")
public class BaseApplicationTest {

    @Value("${server.port}")
    private String serverPort;

    @Autowired
    @Qualifier("testAddonConfiguration")
    private AddonsConfiguration.AddonConfiguration testAddonConfiguration;

    @Autowired
    @Qualifier("rootAddonConfiguration")
    private AddonsConfiguration.AddonConfiguration rootAddonConfiguration;

    protected MockMvc mvc;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected AtlassianHostRepository hostRepository;

    @Autowired
    private AddonDescriptorLoaderRegistry addonDescriptorLoaderRegistry;

    @Before
    public void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
    }

    protected String getServerAddress() {
        return "http://localhost:" + serverPort;
    }

    protected String getAddonBaseUrl() {
        return getServerAddress() + getTestAddonContextPath();
    }

    protected void setJwtAuthenticatedPrincipal(AtlassianHost host) {
        AtlassianHostUser hostUser = new AtlassianHostUser(host, Optional.empty());
        SecurityContextHolder.getContext().setAuthentication(new JwtAuthentication(hostUser, null));
    }

    protected AtlassianHost createHost(String clientKey, String addonKey, String sharedSecret, String baseUrl) {
        AtlassianHost host = new AtlassianHost();
        host.setClientKey(clientKey);
        host.setAddonKey(addonKey);
        host.setSharedSecret(sharedSecret);
        host.setBaseUrl(baseUrl);
        host.setProductType("some-product");
        return host;
    }

    protected AtlassianHost getDefaultHost() {
        return createHost("some-host", testAddonConfiguration.getKey(), "some-secret", "http://example.com/product");
    }

    public AddonsConfiguration.AddonConfiguration getTestAddonConfiguration() {
        return testAddonConfiguration;
    }

    public AddonsConfiguration.AddonConfiguration getRootAddonConfiguration() {
        return rootAddonConfiguration;
    }

    protected String getTestAddonKey() {
        return testAddonConfiguration.getKey();
    }

    protected String getTestAddonName() {
        return testAddonConfiguration.getName();
    }

    protected String getTestAddonContextPath() {
        return testAddonConfiguration.getContextPath();
    }

    protected AddonDescriptorLoader getTestAddonDescriptorLoader() {
        return getAddonDescriptorLoader(getTestAddonContextPath());
    }

    protected String getRootAddonKey() {
        return rootAddonConfiguration.getKey();
    }

    protected String getRootAddonContextPath() {
        return rootAddonConfiguration.getContextPath();
    }

    protected AddonDescriptorLoader getRootAddonDescriptorLoader() {
        return getAddonDescriptorLoader(getRootAddonContextPath());
    }

    protected AtlassianHost saveHost(AtlassianHost host) {
        hostRepository.save(host);
        return host;
    }

    protected AtlassianHost createAndSaveDefaultHost() {
        return saveHost(getDefaultHost());
    }

    private AddonDescriptorLoader getAddonDescriptorLoader(String contextPath) {
        Optional<AddonDescriptorLoader> addonDescriptorLoader = addonDescriptorLoaderRegistry.getByPath(contextPath);
        if (addonDescriptorLoader.isPresent()) {
            return addonDescriptorLoader.get();
        } else {
            throw new RuntimeException("Unable to load addon descriptor for " + getTestAddonContextPath());
        }
    }
}
