package com.atlassian.connect.spring.test.util;

import com.atlassian.connect.spring.AtlassianHost;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.client.ClientHttpRequestInterceptor;

import java.util.Optional;

import static java.util.Collections.singletonList;

public class SimpleJwtSigningRestTemplate extends TestRestTemplate {

    public SimpleJwtSigningRestTemplate(String jwt, AtlassianHost host) {
        this(new FixedJwtSigningClientHttpRequestInterceptor(jwt, host));
    }

    public SimpleJwtSigningRestTemplate(AtlassianHost host, String baseUrl, Optional<String> optionalSubject) {
        this(host.getClientKey(), host.getSharedSecret(), baseUrl, optionalSubject);
    }

    public SimpleJwtSigningRestTemplate(String clientKey, String sharedSecret, String baseUrl, Optional<String> optionalSubject) {
        this(new SimpleJwtSigningClientHttpRequestInterceptor(clientKey, sharedSecret, baseUrl, optionalSubject));
    }

    protected SimpleJwtSigningRestTemplate(ClientHttpRequestInterceptor requestInterceptor) {
        setInterceptors(singletonList(requestInterceptor));
    }
}
