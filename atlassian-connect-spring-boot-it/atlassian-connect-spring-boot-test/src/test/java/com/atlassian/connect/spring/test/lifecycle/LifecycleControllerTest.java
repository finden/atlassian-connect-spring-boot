package com.atlassian.connect.spring.test.lifecycle;

import com.atlassian.connect.spring.AddonInstalledEvent;
import com.atlassian.connect.spring.AddonUninstalledEvent;
import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.it.AddonLifecycleEventCollector;
import com.atlassian.connect.spring.test.util.BaseApplicationTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class LifecycleControllerTest extends BaseApplicationTest {

    @Value("${security.user.name}")
    private String adminUserName;

    @Value("${security.user.password}")
    private String adminUserPassword;

    @Autowired
    private AddonLifecycleEventCollector addonLifecycleEventCollector;

    @Before
    public void setUp() {
        addonLifecycleEventCollector.clearEvents();
    }

    private String getInstalledUrl() {
        return getTestAddonContextPath() + getTestAddonConfiguration().getInstalledUrl();
    }

    private String getUninstalledUrl() {
        return getTestAddonContextPath() + getTestAddonConfiguration().getUninstalledUrl();
    }

    @Test
    public void shouldStoreHostOnFirstInstall() throws Exception {
        mvc.perform(postInstalled(getInstalledUrl(), getDefaultHost().getSharedSecret()))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = getAtlassianHost();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(getDefaultHost().getSharedSecret()));
        assertThat(installedHost.getBaseUrl(), is(getDefaultHost().getBaseUrl()));
        assertThat(installedHost.isAddonInstalled(), is(true));
    }

    @Test
    public void shouldFireEventOnInstall() throws Exception {
        mvc.perform(postInstalled(getInstalledUrl(), getDefaultHost().getSharedSecret()))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = getAtlassianHost();
        AddonInstalledEvent installedEvent = addonLifecycleEventCollector.takeInstalledEvent();
        assertThat(installedEvent.getHost(), equalTo(installedHost));
    }

    @Test
    public void shouldUpdateSharedSecretOnSignedSecondInstall() throws Exception {
        AtlassianHost host = createAndSaveDefaultHost();
        String newSharedSecret = "some-other-secret";
        setJwtAuthenticatedPrincipal(host);
        mvc.perform(postInstalled(getInstalledUrl(), newSharedSecret))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = getAtlassianHost();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(newSharedSecret));
    }

    @Test
    public void shouldRejectSecondInstallWithoutJwt() throws Exception {
        AtlassianHost host = createAndSaveDefaultHost();
        mvc.perform(postInstalled(getInstalledUrl(), host.getSharedSecret()))
                .andExpect(status().isUnauthorized())
                .andExpect(header().string("WWW-Authenticate", String.format("JWT realm=\"%s\"", getTestAddonKey())));
    }

    @Test
    public void shouldRejectSharedSecretUpdateByOtherHost() throws Exception {
        createAndSaveDefaultHost();
        AtlassianHost otherHost = createHost("other-host", "other.addon.key", "other-secret", "http://other-example.com");
        setJwtAuthenticatedPrincipal(otherHost);
        mvc.perform(postInstalled(getInstalledUrl(), "some-other-secret")
                .with(httpBasic(adminUserName, adminUserPassword)))
                .andExpect(status().isForbidden());    }

    @Test
    public void shouldRejectUninstallByOtherHost() throws Exception {
        AtlassianHost host = createAndSaveDefaultHost();
        AtlassianHost otherHost = createHost("other-host", "other.addon.key", "other-secret", "http://other-example.com");
        setJwtAuthenticatedPrincipal(otherHost);
        mvc.perform(postUninstalled(getUninstalledUrl(), host.getSharedSecret())
                .with(httpBasic(adminUserName, adminUserPassword)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldSoftDeleteHostOnUninstall() throws Exception {
        AtlassianHost host = createAndSaveDefaultHost();
        setJwtAuthenticatedPrincipal(host);
        mvc.perform(postUninstalled(getUninstalledUrl(), host.getSharedSecret()))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = getAtlassianHost();
        assertThat(installedHost, notNullValue());
        assertThat(installedHost.getSharedSecret(), is(host.getSharedSecret()));
        assertThat(installedHost.getBaseUrl(), is(host.getBaseUrl()));
        assertThat(installedHost.isAddonInstalled(), is(false));
    }

    @Test
    public void shouldFireEventOnUninstall() throws Exception {
        AtlassianHost host = createAndSaveDefaultHost();
        setJwtAuthenticatedPrincipal(host);
        mvc.perform(postUninstalled(getUninstalledUrl(), host.getSharedSecret()))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = getAtlassianHost();
        AddonUninstalledEvent uninstalledEvent = addonLifecycleEventCollector.takeUninstalledEvent();
        assertThat(uninstalledEvent.getHost(), equalTo(installedHost));
    }

    @Test
    public void shouldIgnoreMissingHostOnUninstall() throws Exception {
        mvc.perform(postUninstalled(getUninstalledUrl(), getDefaultHost().getSharedSecret()))
                .andExpect(status().isNoContent());

        AtlassianHost installedHost = getAtlassianHost();
        assertThat(installedHost, nullValue());
    }

    @Test
    public void shouldRejectSharedSecretUpdateWithBasicAuth() throws Exception {
        createAndSaveDefaultHost();
        mvc.perform(postInstalled(getInstalledUrl(), "some-other-secret")
                .with(httpBasic(adminUserName, adminUserPassword)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldRejectUninstallWithBasicAuth() throws Exception {
        AtlassianHost host = createAndSaveDefaultHost();
        mvc.perform(postUninstalled(getUninstalledUrl(), host.getSharedSecret())
                .with(httpBasic(adminUserName, adminUserPassword)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldRejectInstallWithoutBody() throws Exception {
        mvc.perform(post(getInstalledUrl())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectInstallWithInvalidBody() throws Exception {
        mvc.perform(post(getInstalledUrl())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectInstallWithInvalidEventType() throws Exception {
        mvc.perform(post(getInstalledUrl())
                .contentType(MediaType.APPLICATION_JSON)
                .content(createLifecycleJson("uninstalled", getDefaultHost())))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectUninstallWithoutBody() throws Exception {
        mvc.perform(post(getUninstalledUrl())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectUninstallWithInvalidBody() throws Exception {
        mvc.perform(post(getUninstalledUrl())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRejectUninstallWithInvalidEventType() throws Exception {
        mvc.perform(post(getUninstalledUrl())
                .contentType(MediaType.APPLICATION_JSON)
                .content(createLifecycleJson("installed", getDefaultHost())))
                .andExpect(status().isBadRequest());
    }

    private MockHttpServletRequestBuilder postInstalled(String path, String secret) throws Exception {
        // Always set the servlet path as well so that addon detection works
        return postLifecycleCallback(path, "installed", secret).servletPath(path);
    }

    private MockHttpServletRequestBuilder postUninstalled(String path, String secret) throws Exception {
        // Always set the servlet path as well so that addon detection works
        return postLifecycleCallback(path, "uninstalled", secret).servletPath(path);
    }

    private MockHttpServletRequestBuilder postLifecycleCallback(String path, String eventType, String secret) throws JsonProcessingException {
        AtlassianHost host = getDefaultHost();
        host.setSharedSecret(secret);
        return post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content(createLifecycleJson(eventType, host));
    }

    private String createLifecycleJson(String eventType, AtlassianHost host) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(LifecycleBodyHelper.createLifecycleEventMap(eventType, host));
    }

    private AtlassianHost getAtlassianHost() {
        return hostRepository.findOneByClientKeyAndAddonKey(getDefaultHost().getClientKey(), getTestAddonDescriptorLoader().getDescriptor().getKey()).orElse(null);
    }

    @After
    public void cleanup() {
        hostRepository.deleteAll();
    }
}
