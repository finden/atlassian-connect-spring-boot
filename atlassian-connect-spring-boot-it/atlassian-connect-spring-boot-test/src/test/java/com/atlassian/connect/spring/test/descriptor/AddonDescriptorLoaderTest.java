package com.atlassian.connect.spring.test.descriptor;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.test.util.BaseApplicationTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.startsWith;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class AddonDescriptorLoaderTest extends BaseApplicationTest {

    @Test
    public void shouldReturnRawDescriptor() {
        String rawDescriptor = getTestAddonDescriptorLoader().getRawDescriptor();
        assertThat(rawDescriptor, startsWith("{"));
    }

    @Test
    public void shouldReturnAddonKey() {
        AddonDescriptor descriptor = getTestAddonDescriptorLoader().getDescriptor();
        assertThat(descriptor.getKey(), is(getTestAddonKey()));
    }

    @Test
    public void shouldReturnBaseUrl() {
        assertThat(getTestAddonDescriptorLoader().getDescriptor().getBaseUrl(), is("http://localhost:8081" + getTestAddonContextPath()));
    }

    @Test
    public void shouldReturnAuthenticationType() {
        assertThat(getTestAddonDescriptorLoader().getDescriptor().getAuthenticationType(), is("jwt"));
    }

    @Test
    public void shouldReturnLifecycleUrls() {
        assertThat(getTestAddonDescriptorLoader().getDescriptor().getInstalledLifecycleUrl(), is("/hello-installed"));
        assertThat(getTestAddonDescriptorLoader().getDescriptor().getEnabledLifecycleUrl(), nullValue());
        assertThat(getTestAddonDescriptorLoader().getDescriptor().getDisabledLifecycleUrl(), nullValue());
        assertThat(getTestAddonDescriptorLoader().getDescriptor().getUninstalledLifecycleUrl(), is("/hello-uninstalled"));
    }

}
