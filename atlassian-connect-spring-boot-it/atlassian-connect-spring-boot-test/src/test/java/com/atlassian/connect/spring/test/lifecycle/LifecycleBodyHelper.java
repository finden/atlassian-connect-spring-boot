package com.atlassian.connect.spring.test.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.google.common.collect.ImmutableMap;

public class LifecycleBodyHelper {

    public static ImmutableMap<String, Object> createLifecycleEventMap(String eventType, AtlassianHost host) {
        ImmutableMap.Builder<String, Object> mapBuilder = ImmutableMap.<String, Object>builder()
                .put("key", host.getAddonKey())
                .put("clientKey", host.getClientKey())
                .put("publicKey", "MIGf....ZRWzwIDAQAB")
                .put("serverVersion", "server-version")
                .put("pluginsVersion", "version-of-connect")
                .put("baseUrl", host.getBaseUrl())
                .put("productType", "jira")
                .put("description", "Atlassian JIRA at https://example.atlassian.net")
                .put("serviceEntitlementNumber", "SEN-number")
                .put("eventType", eventType);
        if (eventType.equals("installed")) {
            mapBuilder.put("sharedSecret", host.getSharedSecret());
        }
        return mapBuilder.build();
    }
}
