package com.atlassian.connect.spring.test;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.auth.jwt.JwtAuthentication;
import com.atlassian.connect.spring.test.util.BaseApplicationTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;
import java.util.Optional;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class AtlassianHostRepositoryTest extends BaseApplicationTest {

    private static final String USER_KEY = "charlie";

    @Before
    public void setUp() {
        AtlassianHostUser hostUser = new AtlassianHostUser(null, Optional.of(USER_KEY));
        SecurityContextHolder.getContext().setAuthentication(new JwtAuthentication(hostUser, null));
    }

    @Test
    public void shouldStoreAuditingFields() {
        AtlassianHost host = new AtlassianHost();
        host.setClientKey(randomAlphanumeric(32));
        host.setAddonKey(randomAlphanumeric(32));
        host.setSharedSecret(getDefaultHost().getSharedSecret());
        host.setBaseUrl(getDefaultHost().getBaseUrl());
        host.setProductType("some-product");
        hostRepository.save(host);

        AtlassianHost readHost = hostRepository.findOne(host.getId());
        assertThat(readHost.getCreatedDate(), is(notNullValue()));
        assertThat(readHost.getCreatedBy(), is(notNullValue()));
        assertThat(readHost.getLastModifiedDate(), is(notNullValue()));
        assertThat(readHost.getLastModifiedBy(), is(notNullValue()));
    }

    @Test
    public void shouldGenerateCompositeKeyOnMultiSave() {
        AtlassianHost host1 = createHost("client1", "addon1", "secret", "baseurl");
        AtlassianHost host2 = createHost("client2", "addon2", "secret", "baseurl");

        hostRepository.save(Arrays.asList(host1, host2));

        assertThat(host1.getId(), notNullValue());
        assertThat(host2.getId(), notNullValue());
    }
}
