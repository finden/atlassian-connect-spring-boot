package com.atlassian.connect.spring.test.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.request.jwt.JwtSigningClientHttpRequestInterceptor;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;

import java.net.URI;
import java.util.Optional;

public class FixedJwtSigningClientHttpRequestInterceptor extends JwtSigningClientHttpRequestInterceptor {

    private final String jwt;
    private final AtlassianHost host;

    public FixedJwtSigningClientHttpRequestInterceptor(String jwt, AtlassianHost host) {
        this.jwt = jwt;
        this.host = host;
    }

    @Override
    protected AddonDescriptor getAddonDescriptor() {
        // just return null as it's not actually used in this implementation
        return null;
    }

    @Override
    protected Optional<AtlassianHostUser> getHostUserForRequest(HttpRequest request, AddonDescriptor addonDescriptor) {
        return Optional.of(new AtlassianHostUser(host, Optional.empty()));
    }

    @Override
    protected String createJwt(HttpMethod method, URI uri, AtlassianHostUser hostUser, AddonDescriptor addonDescriptor) {
        return jwt;
    }
}
